import numpy as np
import cv2
import time
import ConfigParser
import ast
import mysql.connector
import thread
import os


def load_config():
    global new_height, new_width, yellowLower, yellowLower_no, yellowUpper, yellowUpper_no
    global greenLower, greenUpper, black_threshold, detect_line, display_size, font_size, green_threshold
    global col_black, col_green, col_potato, download_iteration, upload_iteration
    global temp_save_image, temp_save_video, save_image_period, save_video_period, save_video, save_image

    config = ConfigParser.RawConfigParser()
    config.read('config.cfg')
    new_width = ast.literal_eval(config.get('config', 'new_width'))
    new_height = ast.literal_eval(config.get('config', 'new_height'))

    yellowLower = ast.literal_eval(config.get('config', 'yellowLower'))
    yellowUpper = ast.literal_eval(config.get('config', 'yellowUpper'))

    yellowLower_no = ast.literal_eval(config.get('config', 'yellowLower_no'))
    yellowUpper_no = ast.literal_eval(config.get('config', 'yellowUpper_no'))

    # green_threshold = ast.literal_eval(config.get('config', 'green_threshold'))
    # greenLower, greenUpper = get_green_threshold(green_threshold)

    detect_line = ast.literal_eval(config.get('config', 'detect_line'))
    # black_threshold = ast.literal_eval(config.get('config', 'black_threshold'))

    display_size = ast.literal_eval(config.get('config', 'display_size'))

    font_size = ast.literal_eval(config.get('config', 'font_size'))

    col_potato = col[ast.literal_eval(config.get('config', 'color_potato'))]
    col_green = col[ast.literal_eval(config.get('config', 'color_green'))]
    col_black = col[ast.literal_eval(config.get('config', 'color_black'))]

    # upload_iteration = ast.literal_eval(config.get('config', 'upload_iteration'))
    # download_iteration = ast.literal_eval(config.get('config', 'download_iteration'))

    temp_save_image = ast.literal_eval(config.get('config', 'temp_save_image'))
    temp_save_video = ast.literal_eval(config.get('config', 'temp_save_video'))

    save_image_period = ast.literal_eval(config.get('config', 'save_image_period'))
    save_video_period = ast.literal_eval(config.get('config', 'save_video_period'))

    save_image = ast.literal_eval(config.get('config', 'save_image'))
    save_video = ast.literal_eval(config.get('config', 'save_video'))


def get_green_threshold(thres):
    greenLower = (40 - int(thres / 20), 81 - int(thres / 2.5), 102 - int(thres / 2.5))
    greenUpper = (41 + int(thres / 20), 160 + int(thres / 2.5), 170 + int(thres / 2.5))
    return greenLower, greenUpper


def get_uncontain_contours(contours, hierarchy):
    new_contours = []
    new_hierachy = []
    for i in range(len(contours)):
        if hierarchy[0][i][3] == -1:
            if cv2.contourArea(contours[i]) > 100:
                new_contours.append(contours[i])
                new_hierachy.append(hierarchy[0][i])

        else:
            if cv2.contourArea(contours[i]) > 100 or cv2.arcLength(contours[i], True) > 100:
                new_contours.append(contours[i])
                new_hierachy.append(hierarchy[0][i])

    return new_contours, [new_hierachy]


def get_distance(p1, p2):
    return int(np.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2))


def get_distance_list(p1, p_list2):
    ret = []
    for i in range(len(p_list2)):
        ret.append(int(np.sqrt((p1[0] - p_list2[i][0]) ** 2 + (p1[1] - p_list2[i][1]) ** 2)))
    return ret


def get_centers(conts):
    center_list = []
    for c in conts:
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        center_list.append([cX, cY])

    return center_list


def get_areas(conts):
    area_list = []
    for c in conts:
        area_list.append(cv2.contourArea(c))

    return area_list


def find_potatoes(img_org, img_mask):
    # ----------------------- find contour and delete inner contour ----------------
    regions, contours, hierarchy = cv2.findContours(img_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    new_contours, new_hierarchy = get_uncontain_contours(contours, hierarchy)

    ok_cont = []

    for j in range(50):
        fail_cont = []

        if j == 1:
            contours_first = new_contours
        elif j == 3:
            contours_next = new_contours

        if len(new_contours) == 0:
            break

        for i in range(len(new_contours)):
            area = cv2.contourArea(new_contours[i])
            perimeter = cv2.arcLength(new_contours[i], True)
            if perimeter > 0:
                solidity = float(area) / perimeter / perimeter

            if 50 < area < 500 and solidity > 0.05 and new_hierarchy[0][i][3] == -1:
                ok_cont.append(new_contours[i])
            else:
                fail_cont.append(new_contours[i])

        img_black2 = np.zeros([new_height, new_width], dtype=np.uint8)
        cv2.drawContours(img_black2, fail_cont, -1, 255, -1)

        # -------------------------- erode and dilate --------------------------
        kernel = np.ones((3, 3), dtype=np.uint8)

        if j % 2 == 0:
            mask = cv2.erode(img_black2, kernel, iterations=2)
            mask = cv2.dilate(mask, kernel, iterations=1)
        else:
            mask = cv2.blur(img_black2, (4, 4))
            mask = cv2.threshold(mask, 200, 255, cv2.THRESH_BINARY)[1]
        _, new_contours, new_hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    img_black2 = np.zeros([new_height, new_width], dtype=np.uint8)
    mask_next = cv2.drawContours(img_black2, contours_next, -1, 255, -1)

    return None, ok_cont, contours_first, mask_next


def get_counts_potatoes(contours):
    global center_list_total
    global move_speed

    center_list = get_centers(contours)

    # find the closest points using total list
    find_closest = []
    move_list = []
    for i in range(len(center_list_total)):
        dist_list = get_distance_list(center_list_total[i][-1], center_list)
        if min(dist_list) < 20:
            arg_min = np.argmin(dist_list)
            move_list.append(center_list[arg_min][1] - center_list_total[i][-1][1])
            center_list_total[i].append(center_list[arg_min])
            center_list.pop(arg_min)
            find_closest.append(True)
        else:
            find_closest.append(False)

    if len(move_list) > 0:
        move_speed = int(sum(move_list) * 1.1 / len(move_list))
    else:
        move_speed = 0

    for i in range(len(center_list_total)):
        if not find_closest[i]:
            center_list_total[i].append([center_list_total[i][-1][0], center_list_total[i][-1][1] + move_speed])

    # add new points to total
    for i in range(len(center_list)):
        if detect_line/2 < center_list[i][1] < detect_line:
            center_list_total.append([center_list[i]])

    # remove duplicated points
    for i in range(len(center_list_total)):
        for j in range(i+1, len(center_list_total)):
            if get_distance(center_list_total[i][-1], center_list_total[j][-1]) < 10:
                center_list_total.pop(j)
                break

    # remove end points
    remove_cnt = 0
    for i in range(len(center_list_total), 0, -1):
        if center_list_total[i-1][-1][1] > detect_line:
            center_list_total.pop(i-1)
            remove_cnt += 1

    return center_list, center_list_total, remove_cnt


def get_counts_green(contours_green):
    global center_list_green, move_speed
    center_green = get_centers(contours_green)
    area_green = get_areas(contours_green)
    area_thresh = 20

    for i in range(len(center_list_green)):
        f_add = False
        for j in range(len(center_green)):
            if area_green[j] > area_thresh and get_distance(center_list_green[i][-1], center_green[j]) < 40:
                center_list_green[i].append(center_green[j])
                center_green.pop(j)
                f_add = True
                break

        if not f_add:
            center_list_green[i].append([center_list_green[i][-1][0], center_list_green[i][-1][1] + move_speed])

    for i in range(len(center_green)):
        if area_green[i] > area_thresh and center_green[i][1] < detect_line:
            center_list_green.append([center_green[i]])

    remove_cnt = 0
    for i in range(len(center_list_green), 0, -1):
        f_pop = False
        for j in range(i-1):
            if get_distance(center_list_green[i - 1][-1], center_list_green[j][-1]) < 40:
                center_list_green.pop(i - 1)
                f_pop = True
                break

        if not f_pop:
            if center_list_green[i - 1][-1][1] > detect_line:
                center_list_green.pop(i - 1)
                remove_cnt += 1

    return remove_cnt


def get_counts_black(contours_black):
    global center_list_black
    center_black = get_centers(contours_black)

    cnt_black = 0
    for i in range(len(center_list_black)):
        for j in range(len(center_black)):
            if detect_line-20 < center_list_black[i][1] < detect_line <= center_black[j][1] < detect_line+20 and \
                            get_distance(center_list_black[i], center_black[j]) < 20:
                cnt_black += 1

    center_list_black = center_black

    return cnt_black


def find_blue(img_org,  img_mask):
    # -------------------------- find contour for blue area ------------------------
    _, contours, hierarchy = cv2.findContours(img_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    ret_contours = []
    for i in range(len(contours)):
        if cv2.contourArea(contours[i]) > 20 and hierarchy[0][i][3] == -1:
            ret_contours.append(contours[i])
            cv2.drawContours(img_org, [contours[i]], -1, (0, 255, 0), 2)

    return img_org, ret_contours


def find_black(img_org,  img_mask):
    # -------------------------- find contour for blue area ------------------------
    _, contours, hierarchy = cv2.findContours(img_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    ret_contours = []
    for i in range(len(contours)):
        if cv2.contourArea(contours[i]) > 20 and hierarchy[0][i][3] == -1:
            ret_contours.append(contours[i])
            cv2.drawContours(img_org, [contours[i]], -1, (255, 0, 0), 2)

    return img_org, ret_contours


def write_result(img, font_size, cnt_potatoes, cnt_green, cnt_black):
    if cnt_potatoes > 0:
        str_percent_green = "%.1f" % (100.0 * cnt_green / cnt_potatoes) + '%'
        str_percent_black = "%.1f" % (100.0 * cnt_black / cnt_potatoes) + '%'
    else:
        str_percent_green = "0.0%"
        str_percent_black = "0.0%"

    cv2.rectangle(img, (0, 0),
                  (int(230 * font_size) + int(5 * font_size), int(105 * font_size) + 10 + int(5 * font_size)),
                  (255, 0, 0), -1)

    cv2.rectangle(img, (int(5 * font_size), int(5 * font_size)),
                  (int(230 * font_size), int(105 * font_size) + 10), (255, 255, 255), -1)

    cv2.putText(img, str(cnt_potatoes) + 'x',
                (int(max(5 - len(str(cnt_potatoes)), 0) * font_size * 20 + 5), int(35 * font_size)),
                cv2.FONT_HERSHEY_DUPLEX, font_size, (0, 0, 255), int(font_size))

    cv2.putText(img, str(cnt_green) + 'x',
                (int(max(5 - len(str(cnt_green)), 0) * font_size * 20 + 5), int(70 * font_size)),
                cv2.FONT_HERSHEY_DUPLEX, font_size, (0, 200, 0), int(font_size))

    cv2.putText(img, str(cnt_black) + 'x',
                (int(max(5 - len(str(cnt_black)), 0) * font_size * 20 + 5), int(105 * font_size)),
                cv2.FONT_HERSHEY_DUPLEX, font_size, (0, 0, 0), int(font_size))

    cv2.putText(img, str_percent_green,
                (int(5 - len(str_percent_green) * font_size * 20 + 220 * font_size), int(70 * font_size)),
                cv2.FONT_HERSHEY_DUPLEX, font_size, (0, 200, 0), int(font_size))

    cv2.putText(img, str_percent_black,
                (int(5 - len(str_percent_black) * font_size * 20 + 220 * font_size), int(105 * font_size)),
                cv2.FONT_HERSHEY_DUPLEX, font_size, (0, 0, 0), int(font_size))

    return img


def main_process(img_org, disp_rate=0, upload_sql_server=False):
    img_crop = img_org[150:, 30:]
    img_small = cv2.resize(img_crop, (new_width, new_height))
    # img_center = img_small.copy()
    # cv2.imshow('img_small', img_small)

    img_hsv = cv2.cvtColor(img_small, cv2.COLOR_BGR2HSV)
    # cv2.imshow('img_hsv', img_hsv)

    # ------------------------- get potatoes ------------------------------------
    mask_potato = cv2.inRange(img_hsv, yellowLower, yellowUpper)
    mask_potato_no = cv2.inRange(img_hsv, yellowLower_no, yellowUpper_no)

    mask_potato = mask_potato - mask_potato_no
    # img_potato, contour_potato, contour_first, mask_next = find_potatoes(img_small.copy(), mask_potato)
    _, contour_potato, contour_first, mask_next = find_potatoes(None, mask_potato)

    # ------------------------- get blue ------------------------------------
    mask_blue = cv2.inRange(img_hsv, greenLower, greenUpper)
    # img_blue, contour_blue = find_blue(img_small.copy(), mask_blue)
    mask_blue = cv2.bitwise_and(mask_next, mask_blue)
    _, contour_blue = find_blue(img_small.copy(), mask_blue)

    # ------------------------- get black ------------------------------------
    img_gray = cv2.cvtColor(img_small, cv2.COLOR_BGR2GRAY)
    _, mask_black = cv2.threshold(img_gray, black_threshold, 255, cv2.THRESH_BINARY_INV)
    mask_black = cv2.bitwise_and(mask_next, mask_black)
    # img_black, contour_black = find_black(img_small.copy(), mask_black)
    _, contour_black = find_black(img_small.copy(), mask_black)

    # -------------------- get counts potatoes --------------------------------
    global potatoes_cnt
    # center_list, center_list_total, remove_cnt = get_counts(contour_potato)
    _, _, remove_cnt = get_counts_potatoes(contour_potato)
    potatoes_cnt += remove_cnt

    # ----------------------- get counts green ---------------------------------
    global potatoes_green_cnt
    green_cnt = get_counts_green(contour_blue)
    potatoes_green_cnt += green_cnt

    # list1 = []
    # for i in range(len(center_list_green)):
    #     list1.append(center_list_green[i][-1])
    #     cv2.circle(img_small, (center_list_green[i][-1][0], center_list_green[i][-1][1]), 10, (255, 255, 0), -1)
    # print list1

    # ----------------------- get counts black ---------------------------------
    global potatoes_black_cnt
    black_cnt = get_counts_black(contour_black)
    potatoes_black_cnt += float(black_cnt)/2

    # ----------------------- upload to server --------------------------------
    if upload_sql_server:
        print "upload"
        thread.start_new_thread(thread_server_write, ('thread', potatoes_cnt, potatoes_green_cnt, int(potatoes_black_cnt)))

    # ---------------------------- drawing ------------------------------------
    cv2.drawContours(img_small, contour_first, -1, col_potato, 2)
    cv2.drawContours(img_small, contour_blue, -1, col_green, 2)
    cv2.drawContours(img_small, contour_black, -1, col_black, 2)
    cv2.line(img_small, (0, detect_line), (new_width, detect_line), (255, 0, 0), 2)

    if disp_rate == 0:
        img_ret = write_result(img_small, font_size, potatoes_cnt, potatoes_green_cnt, int(potatoes_black_cnt))

    else:
        img_org = cv2.resize(img_small, (int(1920 * disp_rate), int(1080 * disp_rate)))
        img_ret = write_result(img_org, font_size, potatoes_cnt, potatoes_green_cnt, int(potatoes_black_cnt))

    return img_ret


def thread_server_write(thread_name, cnt_potatoes, cnt_green, cnt_black):
    global green_threshold, black_threshold

    x = cnx.cursor()
    x.execute("""INSERT INTO opencv_sorter01_output (`count_potatoes`, `count_green`, `value_green`, `count_black`, `value_black`)
        VALUES (%s, %s, %s, %s, %s)""", (cnt_potatoes, cnt_green, green_threshold, cnt_black, black_threshold))
    cnx.commit()


def thread_server_read(thread_name, ind):
    x = cnx_read.cursor()
    x.execute("""SELECT * FROM `opencv_sorter01_input`""")
    ret = x.fetchall()[-1]

    global greenUpper, greenLower, black_threshold, download_iteration, upload_iteration, green_threshold
    # greenLower, greenUpper = get_green_threshold(100)
    # black_threshold = 100
    green_threshold = ret[2]
    greenLower, greenUpper = get_green_threshold(green_threshold)
    black_threshold = ret[3]
    upload_iteration = ret[4]
    download_iteration = ret[5]


def trans(conts, para1, para2):
    for i in range(len(conts)):
        for j in range(len(conts[i])):
            conts[i][j][0][0] = conts[i][j][0][0] * para1 + para2[0]
            conts[i][j][0][1] = conts[i][j][0][1] * para1 + para2[1]

    return conts


if __name__ == '__main__':

    f_video = True
    # f_video = False

    if f_video:
        file_name = 'p_video.mp4'
    else:
        file_name = 'p_image1.png'

    # ------------------------ camera setting ----------------------------------
    cap = cv2.VideoCapture(file_name)
    fource = cv2.VideoWriter_fourcc(*"XVID")
    # fource = cv2.VideoWriter_fourcc(*"MJPG")

    # -------------------- variable initialization ------------------------------
    download_iteration = 0
    upload_iteration = 0
    save_image_period = 0
    temp_save_image = ''
    save_video_period = 0
    temp_save_video = ''
    save_image = 0
    save_video = 0

    col = {'red': (0, 0, 255),
           'blue': (255, 0, 0),
           'green': (0, 255, 0),
           'black': (0, 0, 0),
           'white': (255, 255, 255),
           'dark_green': (0, 200, 0)}
    center_list_total = []
    center_list_green = []
    center_list_black = []
    potatoes_cnt = 0
    potatoes_green_cnt = 0
    potatoes_black_cnt = 0
    frame_cnt = 0
    move_speed = 0
    f_stop = False

    # --------------------------- database setting ----------------------------
    cnx = mysql.connector.connect(user='opencv_sorter01',
                                  password='LWVE5juluGfZ2TEk',
                                  host='agroaktiv.de',
                                  database='agroaktiv')
    cnx_read = mysql.connector.connect(user='opencv_sorter01',
                                  password='LWVE5juluGfZ2TEk',
                                  host='agroaktiv.de',
                                  database='agroaktiv')

    # ------------------------- load config data -----------------------------
    load_config()
    if not os.path.isdir(temp_save_image):
        os.makedirs(temp_save_image)
    if not os.path.isdir(temp_save_video):
        os.makedirs(temp_save_video)

    print "Read config data from MySQL Server"
    thread_server_read('', 0)

    print "Processing ..."
    time_up = time.time()
    time_down = time.time()
    time_image = time.time()
    time_video = 0

    while True:
        frame_cnt += 1
        if not f_stop:
            ret, frame = cap.read()

            if frame_cnt % 2 == 0:
                continue

            if ret:
                if time.time() > time_up + upload_iteration:
                    time_up = time.time()
                    upload_server = True
                else:
                    upload_server = False

                img_ret = main_process(frame, disp_rate=display_size, upload_sql_server=upload_server)

                if time.time() > time_down + download_iteration:
                    time_down = time.time()
                    thread.start_new_thread(thread_server_read, ('thread', 1))

            elif not ret and f_video:
                break

            # ---------------------- save image and video ------------------
            if save_video:
                if time.time() - time_video > save_video_period:
                    time_video = time.time()
                    video_name = temp_save_video + '/temp_video_' + str(int(time_image)) + '.avi'
                    video_writer = cv2.VideoWriter(video_name, fource, 25.0, (img_ret.shape[:2][1], img_ret.shape[:2][0]))

            if save_image:
                if time.time() - time_image > save_image_period:
                    time_image = time.time()
                    cv2.imwrite(temp_save_image + '/temp_img_' + str(int(time_image)) + '.jpg', img_ret)

        if frame_cnt % 30 == 0:
            load_config()

        cv2.imshow("img", img_ret)
        if save_video:
            video_writer.write(img_ret)

        k = cv2.waitKey(1) & 0xff
        if k == 27:
            break
        elif k == ord("q"):
            f_stop = not f_stop

    cap.release()
    if save_video:
        video_writer.release()

    cnx.close
